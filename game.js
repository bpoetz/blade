
/* 
TODO:  structure this better.  Right now we execute this code immediately when the browser parses the script tag in index.
We should probably make it happen when the page loads
*/


var canvas = document.getElementById('canvas');
var playerOne, Player;




var MOVE_PIXELS = 10;


function main (){


	if (canvas.getContext) {
		var ctx = canvas.getContext('2d');		
	}
// had to change the way we made the player (no new before) because of the change to the function player.

	playerOne = new Player(ctx, 20, 380);
	playerOne.draw();

    //heres the magic, son
    function clickBinder(e){
      playerOne.moveLeft.call(playerOne); 
    }
	document.getElementById('canvas').addEventListener('click', clickBinder, true);


}
window.addEventListener("load", main, true);


//re-wrote this syntax thinking that would fix it, it's actually not that different from the other syntax we had and I will
//attempt to prove that later but I want to sleep now
function Player (ctx, x, y){
       this.ctx = ctx;
	   this.x = x;
	   this.y = y;
    }
       /* TODO: bryan.  Figure out the best way to structure this */
		/* TODO: make this a method decorator.
 		animate: function (fn){
			this.clear();
			//call the function here
			this.draw();
		}
		*/
    

Player.prototype.moveLeft = function(){
    this.clear();
    this.x = this.x - MOVE_PIXELS;
    this.draw();
}
		/*moveRight: function(){
			this.clear();
			this.x = this.x + MOVE_PIXELS;
			this.draw();
		},

*/
Player.prototype.draw = function(){
  this.ctx.fillRect(this.x, this.y ,100,100);	
}

Player.prototype.clear =  function(){
  this.ctx.clearRect(this.x, this.y, 100,100);
}


